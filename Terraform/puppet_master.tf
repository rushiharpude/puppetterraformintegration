provider "aws" {
  region = "us-east-1"
}


data "aws_vpc" "existing_vpc" {
  id = "vpc-07308c5ef67cdb542"
}


data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-01708e7ac430fc722"
}


data "aws_security_group" "existing_security_group" {
  id = "sg-0c4d6f6c0914185b7"
}

resource "aws_instance" "puppetMaster" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.medium"
  subnet_id     = data.aws_subnet.existing_subnet.id
  key_name      = "Key1"
  associate_public_ip_address = true
  security_groups = [data.aws_security_group.existing_security_group.id]   // Attach security group to instance

   connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("D:\\App Files\\AWS Pem Files\\f=Key1.pem")
    host        = aws_instance.puppetMaster.public_ip
   }
   provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}
